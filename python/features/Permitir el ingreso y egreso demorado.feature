# language: es
# encoding: utf-8

Característica: Permitir el ingreso y egreso de zonas controladas
	Como usuario del sistema de alarma
	Quiero disponer de un tiempo para desactivar el sistema antes de que suene la sirena
	Para poder instalar el teclado de control del sistema de alarma dentro de la casa

Escenario: Salida normal de la casa
	Dado un sistema de alarma instalado, funcional y desarmado
	Cuando armo el sistema en modo ausente
	Entonces una indicación sonora me indica que esta transcurriendo el tiempo de salida
	Cuando transcurren 5 segundos
	Y abro la puerta principal
	Y cierro la puerta principal
	Entonces no se registra una situación de alarma
	Cuando transcurren 40 segundos
	Entonces un indicación sonora me indica que se agotó el tiempo de espera
	Y el sistema queda armado

Escenario: Entrada normal de la casa
	Dado un sistema de alarma instalado, funcional y armado ausente
	Cuando abro la puerta principal
	Entonces una indicación sonora me indica que esta transcurriendo el tiempo de entrada
	Cuando cierro la puerta principal
	Y transcurren 5 segundos
	Y ingreso la clave de desarmado correctamente
	Entonces se silencia la indicación sonora del tiempo de entrada
	Y el sistema queda desarmado