# language: es
# encoding: utf-8

Característica: Permitir el manejo de eventos temporizados en el sistema
    Como desarrollador de sistemas embebidos
    Quiero disponer de una libreria sencilla y confiable
    Para poder gestionar facilmente tiempos de espera en los programas

Escenario: Arranque del sistema
    Cuando configuro la libreria de temporizadores
    Entonces se configura una interrupcion cada decima de segundo
    Y no hay ninguna espera temporizada activa

Escenario: Programacion de un evento de unica vez
    Dada una libreria de temporizadores configurada
    Cuando se programa una cuenta regresiva con el valor 10
    Y transcurren 10 decimas de segundo
    Entonces la libreria genera el evento

Esquema del escenario: Programacion de un evento de unica vez
    Dada una libreria de temporizadores configurada
    Cuando se programa una cuenta regresiva con el valor <cuenta>
    Y transcurren <espera> decimas de segundo
    Entonces la libreria <resultado> genera el evento

    Ejemplos:
    | cuenta | espera | resultado |
    | 10     | 9      | No        |
    | 10     | 10     | Si        |
    | 10     | 11     | Si        |
