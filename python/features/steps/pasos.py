#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from behave import *
from temporizadores import *
from unittest.mock import *


@given('una libreria de temporizadores configurada')
def implementacion(contexto):
    configurar_temporizadores()


@when('configuro la libreria de temporizadores')
@patch('temporizadores.configurar_interrupcion')
def implementacion(contexto, mock):
    contexto.mock = mock
    configurar_temporizadores()


@when('se programa una cuenta regresiva con el valor {valor}')
def implementacion(contexto, valor):
    def prueba():
        print('llamada')
    contexto.funcion = MagicMock(side_effect=prueba)

    programar_cuenta(valor, contexto.funcion)


@when('transcurren {valor} decimas de segundo')
def implementacion(contexto, valor):
    for indice in range(int(valor)):
        incrementar_decima()


@then('se configura una interrupcion cada decima de segundo')
def implementacion(contexto):
    contexto.mock.assert_called_with(100)


@then('no hay ninguna espera temporizada activa')
def implementacion(contexto):
    assert temporizadores_activos() == 0


@then('la libreria genera el evento')
def implementacion(contexto):
    assert contexto.funcion.called


@then('la libreria {resultado} genera el evento')
def implementacion(contexto, resultado):
    if resultado.upper() == 'SI':
        assert contexto.funcion.called
    else:
        assert not contexto.funcion.called
