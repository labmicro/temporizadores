#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from plataforma import *

activos = 0
cuentas = []

def configurar_temporizadores():
    configurar_interrupcion(100)
    cuentas.clear()

def temporizadores_activos():
    return activos


def programar_cuenta(valor, funcion):
    entrada = {'valor': int(valor), 'funcion': funcion}
    cuentas.append(entrada)


def incrementar_decima():
    for cuenta in cuentas:
        cuenta['valor'] -= 1
        if cuenta['valor'] == 0:
            cuenta['funcion']()
            cuentas.remove(cuenta)
