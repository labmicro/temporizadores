#include "unity.h"
#include "temporizadores.h"
#include "mock_plataforma.h"
#include <stdbool.h>

bool llamada;

void prueba_evento(void) {
    llamada = true; 
}

void esperar(uint16_t espera) {
    uint8_t demora;
    for(demora = 0; demora < espera; demora++) {
        incrementar_decima();
    }

}

void setUp(void) {
    llamada = false;
    configurar_interrupcion_Expect(100);
    configurar_temporizadores();
}

void tearDown(void) {
}

void test_configuracion(void) {
    TEST_ASSERT_EQUAL(0, temporizadores_activos());
}

void test_evento_unica_vez(void) {
    programar_cuenta(10, prueba_evento);
    esperar(10);
    TEST_ASSERT(llamada);
}

void test_varias_pruebas_unica_vez(void) {
    const struct ejemplos_s {
        uint16_t cuenta;
        uint16_t espera;
        bool resultado;
    } ejemplos[] = {
        { .cuenta = 10, .espera = 9, .resultado = false },
        { .cuenta = 10, .espera = 10, .resultado = true },
        { .cuenta = 10, .espera = 11, .resultado = true },
    };
    uint8_t indice;
    char mensaje[64];

    for(indice = 0; indice < sizeof(ejemplos) / sizeof(struct ejemplos_s); indice++) {
        sprintf(mensaje, "ejemplo %d", indice);
        setUp();
        programar_cuenta(ejemplos[indice].cuenta, prueba_evento);
        esperar(ejemplos[indice].espera);
        TEST_ASSERT_EQUAL_MESSAGE(ejemplos[indice].resultado, llamada, mensaje);
    }
}