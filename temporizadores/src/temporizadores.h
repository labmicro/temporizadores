#include <stdint.h>

void configurar_temporizadores(void);

uint8_t temporizadores_activos(void);

void programar_cuenta(uint16_t espera, void (*evento)(void));

void incrementar_decima(void);